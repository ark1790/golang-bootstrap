package main

import (
	"log"
	"net/http"
	"os"
	"os/signal"

	"github.com/ark1790/web/data"
	"github.com/ark1790/web/ui"
)

func main() {

	err := data.OpenDBSession("mongodb://localhost:27017/")
	if err != nil {
		panic(err)
	}

	http.Handle("/", ui.Router)

	go func() {
		http.ListenAndServe(":5000", nil)
	}()

	sigChannel := make(chan os.Signal, 1)
	signal.Notify(sigChannel, os.Interrupt)
	log.Println("Signal ", <-sigChannel, "received")

}
