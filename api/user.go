package api

import (
	"fmt"
	"net/http"

	"github.com/ark1790/web/data"
)

// HandleUserLogin ...
func HandleUserLogin(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		panic(err)
	}

	body := struct {
		Handle   string `json:"handle"`
		Password string `json:"password"`
	}{
		Handle:   r.PostFormValue("handle"),
		Password: r.PostFormValue("password"),
	}

	user, err := data.GetUserByHandle(body.Handle)
	if err != nil {
		panic(err)
	}

	fmt.Println(body)
	fmt.Println(user)

	fl, err := user.ComparePassword(body.Password)

	if !fl {
		http.Error(w, "", http.StatusForbidden)
		return
	}
	// fmt.Fprintln(w, user)

	sess, err := data.Store.Get(r, "life")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	sess.Values["self.id"] = user.ID.Hex()
	err = sess.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)

}

// HandleSignUp ...
func HandleSignUp(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		panic(err)
	}

	body := struct {
		Handle   string `json:"handle"`
		Password string `json:"password"`
	}{
		Handle:   r.PostFormValue("handle"),
		Password: r.PostFormValue("password"),
	}

	user := &data.User{}
	user.Handle = body.Handle
	user.Password = body.Password
	user.Level = data.Normal

	err = user.Put()

	if err != nil {
		panic(err)
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)

}
