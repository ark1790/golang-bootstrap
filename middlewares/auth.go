package check

import (
	"log"
	"net/http"

	"github.com/ark1790/web/data"
)

// func IsAuthenticated(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
//
// }
//

// IsAuthenticated ... => just for checking right now
func IsAuthenticated(w http.ResponseWriter, r *http.Request) bool {
	sess, err := data.Store.Get(r, "life")
	if err != nil {
		panic(err)
	}

	id, _ := sess.Values["self.id"]

	if id == nil {
		return false
	}

	log.Println(id)
	return true
}
