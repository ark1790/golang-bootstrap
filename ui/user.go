package ui

import (
	"net/http"

	"github.com/ark1790/web/api"
	"github.com/ark1790/web/middlewares"
)

// HandleUserSession ...
// func HandleUserSession(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
// 	sess, err := data.Store.Get(r, "life")
// 	if err != nil {
// 		panic(err)
// 	}
//
// 	id, _ := sess.Values["self.id"].(string)
// 	if id == "" {
// 		next(w, r)
// 		return
// 	}
//
// 	user, err := data.GetUserByID(id)
// 	if err != nil {
// 		panic(err)
// 	}
// 	if user == nil {
// 		next(w, r)
// 		return
// 	}
// 	context.Set(r, "self", user)
// 	next(w, r)
// }

// ServeSignUp  ...
func ServeSignUp(w http.ResponseWriter, r *http.Request) {
	err := TplSignUp.Execute(w, "")
	if err != nil {
		panic(err)
	}
}

// ServeUserLogin ...
func ServeUserLogin(w http.ResponseWriter, r *http.Request) {
	err := TplLogin.Execute(w, "")
	if err != nil {
		panic(err)
	}
}

// ServeProfile ..
func ServeProfile(w http.ResponseWriter, r *http.Request) {
	lg := check.IsAuthenticated(w, r)
	if !lg {
		http.Redirect(w, r, "/user/login", http.StatusSeeOther)
		return
	}

	err := TplProfile.Execute(w, "")
	if err != nil {
		panic(err)
	}
}

func init() {
	Router.NewRoute().
		Methods("GET").
		Path("/user/login").
		Handler(http.HandlerFunc(ServeUserLogin))

	Router.NewRoute().
		Methods("POST").
		Path("/user/login").
		Handler(http.HandlerFunc(api.HandleUserLogin))

	Router.NewRoute().
		Methods("GET").
		Path("/user/signup").
		Handler(http.HandlerFunc(ServeSignUp))

	Router.NewRoute().
		Methods("POST").
		Path("/user/signup").
		Handler(http.HandlerFunc(api.HandleSignUp))

	Router.NewRoute().
		Methods("GET").
		Path("/user/profile").
		Handler(http.HandlerFunc(ServeProfile))
}
