package ui

import "html/template"

// Layout ...
var Layout = template.Must(template.New("layout.html").ParseFiles("ui/templates/layout.html"))

// TplIndex ...
var TplIndex = template.Must(template.Must(Layout.Clone()).ParseFiles("ui/templates/index.html"))

// TplLogin ...
var TplLogin = template.Must(template.Must(Layout.Clone()).ParseFiles("ui/templates/login.html"))

// TplSignUp ...
var TplSignUp = template.Must(template.Must(Layout.Clone()).ParseFiles("ui/templates/signup.html"))

// TplProfile ...
var TplProfile = template.Must(template.Must(Layout.Clone()).ParseFiles("ui/templates/profile.html"))
