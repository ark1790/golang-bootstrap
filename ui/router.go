package ui

import (
	"net/http"

	"github.com/gorilla/mux"
)

// Router to Handle Requests
var Router = mux.NewRouter()

// ServeIndex ...
func ServeIndex(w http.ResponseWriter, r *http.Request) {
	err := TplIndex.Execute(w, "")
	if err != nil {
		panic(err)
	}
}

func init() {
	Router.NewRoute().
		Methods("GET").
		Path("/").
		Handler(http.HandlerFunc(ServeIndex))
}
