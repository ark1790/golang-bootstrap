package ui

import "net/http"

func init() {
	fs := http.FileServer(http.Dir("./assets/static/"))

	Router.NewRoute().
		Methods("GET").
		PathPrefix("/static/").
		Handler(http.StripPrefix("/static/", fs))
}
