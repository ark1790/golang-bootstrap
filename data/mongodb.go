package data

import "gopkg.in/mgo.v2"

var sess *mgo.Session

const (
	userC = "users"
)

func OpenDBSession(url string) error {
	sess, _ = mgo.Dial(url)

	return nil
}
