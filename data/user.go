package data

import (
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// Defines User Access Level
const (
	Normal        = 0
	Manager       = 1
	Administrator = 2
)

// User Structure
type User struct {
	ID       bson.ObjectId `bson:"_id"`
	Handle   string        `bson:"handle"`
	Password string        `bson:"password"`

	Level int    `bson:"level"`
	Name  string `bson:"name"`

	CreatedAt  time.Time `bson:"createdat"`
	ModifiedAt time.Time `bson:"modifiedat"`
}

// GetUserByID ...
func GetUserByID(id string) (*User, error) {
	user := User{}
	err := sess.DB("").C(userC).FindId(bson.ObjectIdHex(id)).One(&user)
	if err == mgo.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetUserByHandle ...
func GetUserByHandle(handle string) (*User, error) {
	user := User{}
	log.Println(handle)
	err := sess.DB("").C(userC).Find(bson.M{"handle": handle}).One(&user)
	if err == mgo.ErrNotFound {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// ComparePassword => Pore Bcrypt use kora jabe :D
func (u *User) ComparePassword(password string) (bool, error) {
	if password != u.Password {
		return false, nil
	}

	return true, nil
}

// SetPassword => Pore Bcrypt user kora jabe :D
func (u *User) SetPassword(password string) error {
	u.Password = password
	return nil
}

// Put ...
func (u *User) Put() error {
	u.ModifiedAt = time.Now()

	if u.ID == "" {
		u.ID = bson.NewObjectId()
		u.CreatedAt = u.ModifiedAt
	}

	_, err := sess.DB("").C(userC).UpsertId(u.ID, u)
	return err
}
