package data

import (
	"github.com/gorilla/sessions"
)

// Store stores session
var Store sessions.Store

func init() {
	Store = sessions.NewCookieStore([]byte("SECRETS"))
}
